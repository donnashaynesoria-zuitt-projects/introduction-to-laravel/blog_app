<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    //Create a new post

    public function create()
    {
        return view('posts.create');
    }

    //endpoint: POST /posts

    public function store(Request $req)
    {
        // Create a new post object
        $new_post = new Post([
            'title' => $req->input('title'),
            'content' => $req->input('content'),
            'user_id' => Auth::user()->id
        ]);
        // Save it to the database

        $new_post->save();
        // Redirect the user somewhere

        return redirect('/posts/create');
    }

    //GET /posts

    public function index()
    {
        $posts_list=Post::all();
        return view('posts.index')->with('posts', $posts_list);
    }


    // GET /posts/<posts_id>

    public function show($post_id)
    {
        //Retrieve specific posts
        $post = Post::find($post_id);

        return view('posts.show')->with('post', $post);

    }


    //GET /posts/my-posts

    public function myPosts()
    {
        
       $my_posts = Auth::user()->posts;
       return view('posts.index')->with('posts', $my_posts);

    }

    //GET /posts/{post_id}/edit
    public function edit($post_id)
    {
        //Find the post to be updated
        $existing_post = Post::find($post_id);
        //Redirect user to the page where the post will be updated
        return view('posts.edit')->with('post', $existing_post);
    }

    // PUT /posts/{post_id}
    public function update($post_id, Request $req)
    {
        //Find an existing post to be updated
        $existing_post = Post::find($post_id);

        //Set the new values of an existing post
        $existing_post->title = $req->input('title');
        $existing_post->content = $req->input('content');
        $existing_post->save();

        //Redirect the user to the page of individual post
        //double quotes will treat it as variable

        return redirect("/posts/$post_id");
    }

    //DELETE /posts/{post_id}

    public function destroy ($post_id)
    {
        //Find the existing post to be deleted

        $existing_post = Post::find($post_id);

        //Delete the post
        $existing_post->delete();

        //Redirect the user somewhere

        return redirect('/posts');
    }

    // /posts/{post_id}/archive

    public function archive($post_id)

    {
        //Find an existing post to be archived
        $existing_post = Post::find($post_id);

        //Set the new value of the is_active field
        $existing_post->is_active = false;
        $existing_post->save();

        //Redirect the user somewhere. Double quote is needed because there is a variable
        return redirect("/posts/$post_id");
    }

    //GET /posts/my-posts/allArchived
    public function allArchived()
    {
        
       $my_posts = Auth::user()->posts;
       return view('posts.archived')->with('posts', $my_posts);

    }

}
