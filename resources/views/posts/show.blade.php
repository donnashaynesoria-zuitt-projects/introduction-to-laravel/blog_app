@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>{{ $post->title }} <small class="badge badge-pill badge-secondary">{{ $post->is_active ? '' : 'archived' }}</small></h1>
        </div>
        <div class="col-md-8">
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <h3 class="card-title">
                    <a href="#"><small>Posted by: {{ $post->user->name }}</small></a>
                  </h3>
                  <p class="card-text">{{ $post->content }}</p>
                  
                </div>

              </div>
              
              <div class="container">
                  <div class="row">
                    <div class ="col-md-7">
                    <a class="btn btn-link" href="/posts#{{ $post->id }}">Back</a>
                    </div>
                    @if(Auth::check())
                    <div class ="col-md-1 text-right">
                      <form method="GET" action="/posts/{{$post->id}}/edit">
                        @method('PUT')
                        @csrf
                          <button type="submit" class="btn btn-primary">Edit</button>
                      </form>
                    </div>
                      <div class ="col-md-2">
                      <form method="POST" action="/posts/{{$post->id}}">
                        @method('DELETE')
                        @csrf
                          <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                  </div>
                    <div class ="col-md-2">
                    <form method="POST" action="/posts/{{$post->id}}/archive">
                        @method('PUT')
                        @csrf
                          <button type="submit" class="btn btn-warning">Archive</button>
                      </form>

                    </div>
                    @endif
                  </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection